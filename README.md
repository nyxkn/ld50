# (this is not) Dating Advice

*This is my submission for [Ludum Dare 50](https://ldjam.com/events/ludum-dare/50/this-is-not-dating-advice), Compo event. Theme: Delay the inevitable.*

*Play it on [itch.io](https://nyxkn.itch.io/dating-advice).*

## License

### Source code

All source code is licensed under the terms of the [GPL-3.0-only License](https://spdx.org/licenses/GPL-3.0-only.html).

### Assets

All assets (images and audio files) are licensed under the [CC-BY-SA 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/).

This includes everything in the *assets-source*, *media*, and *game/assets* folders.

