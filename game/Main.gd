extends Node


func _ready():
	# entry point for the whole application
	# initialize things and load a different scene

	# custom config settings
	if !OS.is_debug_build():
		Config.log_hide_level = {
			Log.Level.DEBUG: true,
		}

	# load other settings from file
#	Config.loadConfig()
#	Log.i("config loaded", name)

	# example calls for changing scene
	if OS.is_debug_build():
#		Framework.change_scene(Config.scenes.main_menu, false)
		Framework.change_scene(Config.scenes.new_game, false)
#		Framework.change_scene("res://scenes/Intro.tscn", false)
	else:
#		Framework.change_scene(Config.scenes.intro, false)
		Framework.change_scene(Config.scenes.main_menu, false)
