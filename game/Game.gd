extends Node


func _enter_tree() -> void:
	pass


func _ready() -> void:
	Pause.pausing_allowed = true
	setup()


func _exit_tree() -> void:
	pass


func _input(event: InputEvent) -> void:
	pass
#	Utils.log_event(event, "_input", name)
#	if event.is_action_pressed("ui_home"):
#		fall_animation($Stats/You/Money)


func _unhandled_input(event: InputEvent) -> void:
	pass
#	Utils.log_event(event, "_unhandled_input", name)

#	if event.is_action_pressed("ui_home"):
#		Framework.change_scene(Config.scenes.main_menu)


func _gui_input(event: InputEvent) -> void:
	pass
#	Utils.log_event(event, "_gui_input", name)


func _unhandled_key_input(event: InputEventKey) -> void:
	pass
#	Utils.log_event(event, "_unhandled_key_input", name)


var choices := {}
#var stats := {
#	health = 50,
#	money = 50,
#	sexual = 50,
#	intellectual = 50,
#	status = 50,
#	emotional = 50,
#	}

var stats := {
	health = 10,
	money = 10,
	sexual = 10,
	intellectual = 10,
	status = 10,
	emotional = 10,
	}

var stats_gameover := {
	health = "Your own wellbeing has severely deteriorated. Your mind and body have lost all integrity. You have become an empty shell. Your partner notices this.",
	money = "You've run out of money. So irresponsible. How do you plan to feed your family? And what about buying presents? That won't do. You are fired.",
	sexual = "Your sex life was incredibly unengaging. We are talking about starfish level of incompetence. Your partner is thoroughly disappointed and begins looking for greener pastures.",
	intellectual = "The intellectual aspect of your relationship was sorely missing. You didn't have anything interesting to talk about other than laughing at your own farts.",
	status = "All your friends thought you two were very odd and spent way too much time doing strange things by yourselves. Even your mom doesn't call anymore. Your partner can't take this any longer.",
	emotional = "There was no emotional connection between you two. Maybe try to at least pretend like you care. Do you think this is just a game?",
}


var responses := {
	"date": [
		"I had a great time on this date with you! ^^ Thank you for taking me out.",
		"Sorry, I am too busy today. Let's try another time?",
	],
	"gift": [
		"Wow, thank you for this beautiful gift! I was so surprised!",
		"Thanks for yet another gift I guess. I wish you'd do more than just buy me things...",
	],
	"sex": [
		"... ahhh... yes! ooh... mmmm... wait, no, not in there!... mhmmmm, yes, that's better.",
		"I appreciate you trying, but... that was kind of... awkward... I wish you weren't so inexperienced.",
	],
	"travel": [
		"What a beautiful sight that was! It was lovely to spend some time with you away from home.",
		"Well, talk about choosing the most boring place to visit ever... Only you'd be able to do that.",
	],
	"activity": [
		"This was a great way to spend the evening. I like spending time with you this way!",
		"Really, I wish you weren't so incredibly boring. Seriously, who comes up with ideas like this?",
	],
	"work": [
		"It's okay love, I understand. We need the money to get by. And so I can get more presents... right? :)",
		"Really, you're spending time working AGAIN? Why are we even in a relationship... Go and marry your work!",
	],
	"self": [
		"Have fun with your training! I like it when you're in shape. You get all strong and vigorous in bed ;)",
		"You're working out again? Is that even doing anything for you? You look just as fat as when I met you.",
	],
}


onready var rng = Framework.rng

#onready var button_choice1: Button = $Choices/VBoxContainer/Choice1
onready var button_choice1: Button = $Choices/Choice1
#onready var button_choice2: Button = $Choices/VBoxContainer/Choice2
onready var button_choice2: Button = $Choices/Choice2
onready var buttons := [button_choice1, button_choice2]
onready var stats_group = get_tree().get_nodes_in_group("stats")

# id of current choices
var current_choices := []
const CHOICES_NUMBER = 2

var changing_choice = true
var gameover = false
var day = 0
#var help_open = false

#var choice1_y = -4
#onready var choice1_y = button_choice1.rect_position.y
#var choice2_y = 20
#onready var choice2_y = button_choice2.rect_position.y

func setup():
	choices = Data.import_csv("res://assets/data.csv")
	for data in choices.values():
		for k in data:
			if k in stats:
				data[k] = int(data[k])

	# wait for transition end
	$TextBox.always_visible = true
	$TextBox.reset_textbox()
	$TextBox.queue_text("Welcome home darling. What are we going to do today? Please take good care of me ^_^")

	Transition.connect("fade_in_completed", self, "on_fade_in")
	if not Transition.fading:
		on_fade_in()

	update_bg()

	for b in buttons:
		b.rect_position.x = 640


func on_fade_in():
	update_stats_ui()
	yield(get_tree().create_timer(1.0), "timeout")
	new_choices()
	$ChoiceSFX.play()

func new_choices():
	var choices_keys = choices.keys().duplicate()
	var new_choices := []

	choices_keys.shuffle()
	for i in CHOICES_NUMBER:
		var choice = choices_keys.pop_back()
		new_choices.push_back(choice)

	for i in buttons.size():
		var b: Button = buttons[i]
		b.text = choices[new_choices[i]].text

	yield(get_tree(), "idle_frame")

	for b in buttons:
		b.rect_size.x = 20
		var y = b.rect_position.y
		$TweenChoicesIn.interpolate_property(b, "rect_position",
#			Vector2(640, y), Vector2(-b.rect_size.x / 2, y), 0.6, Tween.TRANS_QUART)
			Vector2(640, y), Vector2(-b.rect_size.x / 2, y), 0.6, Tween.TRANS_BACK)


	$TweenChoicesIn.start()
	day += 1
	$Day.value = day
#	changing_choice = true
	current_choices = new_choices



func _on_TweenChoicesOut_tween_all_completed() -> void:
	new_choices()


func check_endgame():
	# this is a post-jam fix and it's pretty hacky cause i wasn't bother to rewrite this properly
	var lowest_stat
	var lowest_stat_icon
	var lowest_value = 0
	for s in stats:
		if stats[s] <= 0:
			var current_icon
			for stat_icon in stats_group:
				if stat_icon.name.to_lower() == s:
					current_icon = stat_icon
					fall_animation(stat_icon)
			if stats[s] <= lowest_value:
				lowest_value = stats[s]
				lowest_stat = s
				lowest_stat_icon = current_icon

	if lowest_stat:
		G.game_over_text = stats_gameover[lowest_stat]
		G.game_over_day = day

		G.game_over_icon = lowest_stat_icon.get_node("TextureRect").duplicate()

		gameover = true

func fall_animation(stat_icon):
	var pos = stat_icon.rect_position
	$TweenFall.interpolate_property(stat_icon, "rect_position", null, Vector2(pos.x, 32), 0.3, Tween.TRANS_SINE, Tween.EASE_IN)
	$TweenFall.start()

	# i don't even know...
	$TweenRotate.interpolate_property(stat_icon, "rect_rotation", null, 35, 0.3, Tween.TRANS_SINE, Tween.EASE_IN_OUT, 0.25)
	$TweenRotate.start()
	yield($TweenRotate, "tween_all_completed")
	$TweenRotate.interpolate_property(stat_icon, "rect_rotation", null, -25, 0.55, Tween.TRANS_SINE)
	$TweenRotate.start()
	yield($TweenRotate, "tween_all_completed")
	$TweenRotate.interpolate_property(stat_icon, "rect_rotation", null, 15, 0.5, Tween.TRANS_SINE)
	$TweenRotate.start()
	yield($TweenRotate, "tween_all_completed")
	$TweenRotate.interpolate_property(stat_icon, "rect_rotation", null, -15, 0.5, Tween.TRANS_SINE)
	$TweenRotate.start()
	yield($TweenRotate, "tween_all_completed")
	$TweenRotate.interpolate_property(stat_icon, "rect_rotation", null, 10, 0.5, Tween.TRANS_SINE)
	$TweenRotate.start()
	yield($TweenRotate, "tween_all_completed")
	$TweenRotate.interpolate_property(stat_icon, "rect_rotation", null, -5, 0.5, Tween.TRANS_SINE)
	$TweenRotate.start()
	yield($TweenRotate, "tween_all_completed")
	$TweenRotate.interpolate_property(stat_icon, "rect_rotation", null, 2, 0.5, Tween.TRANS_SINE)
	$TweenRotate.start()



func check_response(choice_id):
	var choice = choices[choice_id]
	var choice_type = choice.type
	var main_stat = ""

	var main_stat_value = -99
	for k in choice:
		if k in stats:
			if choice[k] > main_stat_value:
				main_stat_value = choice[k]
				main_stat = k

#	print("main stat: " + main_stat)

	var main_stat_node
	for s in stats_group:
		if s.name.to_lower() == main_stat:
			main_stat_node = s

	animate_icon(main_stat_node.get_node("TextureRect"))
#	var main_stat_icon = stats_group[main_stat.capitalize()]

#		get_node(choice_id.to_lower())


	var rnd = randf()
	var global_main_stat = stats[main_stat]
	var normalized_stat = global_main_stat / 100.0
#	print(global_main_stat)
	var rejection_threshold = pow(normalized_stat, 2) * 1.0
#	print(rejection_threshold)

	var response_text
	var response_positive: bool
	if rnd < rejection_threshold:
		response_text = responses[choice_type][1]
		response_positive = false
	else:
		response_text = responses[choice_type][0]
		response_positive = true


#	print("response " + str(response_positive))
#	$Response/Label.text = response
#	print(response)
	$TextBox.queue_text(response_text)

	return response_positive

func animate_icon(main_stat_node):
#	$TweenIcon.interpolate_property(main_stat_node, "rect_scale", null, Vector2(1.05, 1.05), 0.4, Tween.TRANS_QUAD)
	$TweenIcon.interpolate_property(main_stat_node, "rect_scale", null, Vector2(1.1, 1.1), 0.4, Tween.TRANS_QUAD)
	$TweenIcon.start()
	yield($TweenIcon, "tween_all_completed")
	$TweenIcon.interpolate_property(main_stat_node, "rect_scale", null, Vector2(1, 1), 0.4, Tween.TRANS_QUAD)
	$TweenIcon.start()


func make_choice(choice_number):
	changing_choice = true

	var choice_id = current_choices[choice_number]

	var response_positive = check_response(choice_id)

	var choice_stats = choices[choice_id]
	for s in choice_stats:
		if s in stats:
			var value = choice_stats[s]
			# on negative response, the positive values instead remove half of the value
			if not response_positive and value > 0:
				stats[s] -= round(value * 0.5)
			else:
				stats[s] += value

			# if negative response, the negative value are amplified by adding a portion of the value again
#			if value < 0 and not response_positive:
#				stats[s] += round(value * 0.5)

	update_stats_ui()

	check_endgame()
	if gameover:
		G.get_node("GameOver").play()
		yield(get_tree().create_timer(0.5), "timeout")
		Framework.change_scene(Config.scenes.ending, true, 2.0)
		return

	# this has to happen after gameover check
	if response_positive:
		$ResponsePositive.play()
	else:
		$ResponseNegative.play()


	for b in buttons:
#		b.disabled = true
		$TweenChoicesOut.interpolate_property(b, "rect_position",
#			b.rect_position, Vector2(-640, b.rect_position.y), 0.5, Tween.TRANS_EXPO)
			null, Vector2(-640, b.rect_position.y), 0.5, Tween.TRANS_EXPO)

	$TweenChoicesOut.start()

	$ChoiceSFX.play()
	$TextBox.reset_textbox()


func update_bg():
	var stats_sum = 0
	for s in stats:
		var centered = (stats[s] / 100.0 - 1.0)
#		stats_sum += pow(centered, 3)
		stats_sum += pow(centered, 2) * 2.0
#		stats_sum += abs(centered) * 2.0

	var stats_avg = stats_sum / float(stats.size())
#	var stats_avg_around_zero = stats_avg - 0.5

#	var hue = $Bg.color.h
	var new_hue = lerp(0.2, 1.0, min(stats_avg, 1.0))
#	$Bg.color.h = new_hue
	var new_color = $Bg.color
	new_color.h = new_hue
#	$TweenBg.interpolate_property($Bg, "color", $Bg.color, new_color, 1.0)
	$TweenBg.interpolate_property($Bg, "color", null, new_color, 1.0)
	$TweenBg.start()


func update_stats_ui():
#	var stats_group = get_tree().get_nodes_in_group("stats")
	for s in stats_group:
		var stat_name = s.name.to_lower()
		var pbar: TextureProgress = s.get_node("TextureProgress")
#		pbar.value = stats[stat_name]
		$TweenBar.interpolate_property(pbar, "value", pbar.value, stats[stat_name], 1.0, Tween.TRANS_CUBIC)

	$TweenBar.start()

	update_bg()


func _process(delta: float) -> void:
	pass
#	$Debug.print(stats, "stats")


func _on_Choice1_pressed() -> void:
	if not changing_choice:
		make_choice(0)


func _on_Choice2_pressed() -> void:
	if not changing_choice:
		make_choice(1)



func _on_TweenChoicesIn_tween_all_completed() -> void:
	changing_choice = false




func _on_HelpButton_pressed() -> void:
#	help_open = true
	$HelpLayer.show()
	$HelpButton.hide()
	$Day.hide()




func _on_HelpLayer_gui_input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_click"):
		$HelpLayer.hide()
		$HelpButton.show()
		$Day.show()

