#!/bin/sh

# run unoconv --listener if it complains
# docs: https://wiki.openoffice.org/wiki/Documentation/DevGuide/Spreadsheets/Filter_Options#Filter_Options_for_the_CSV_Filter

# or read from $1
file="data.ods"

# alias conv="unoconv -e FilterOptions=59,34,0,1 -f csv $1"
run="unoconv -e FilterOptions=59,34,0,1 -f csv $1"

# run when file changes
echo $file | entr $run /_


