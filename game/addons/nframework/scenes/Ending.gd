extends Control


var keypress_enabled: bool = false


func _ready() -> void:
	G.game_over_icon.visible = false
	$Stat.add_child(G.game_over_icon)
	$Tween.interpolate_property(G.game_over_icon, "rect_scale", Vector2(0, 0), Vector2(1, 1), 1.0, Tween.TRANS_QUART)
#	$Stat.visible = false
#	$Stat/TextureRect.texture = load("res://assets/" + G.game_over_icon + ".png")
#	$Tween.interpolate_property($Stat/TextureRect, "rect_scale", Vector2(0, 0), Vector2(1, 1), 1.0, Tween.TRANS_QUART)
	$Tween.start()
	$Stat.visible = true
	G.game_over_icon.visible = true

	$Desc1.text = G.game_over_text
	$Desc2.text = "Your partner leaves you after " + str(G.game_over_day) + " days."
	yield(get_tree().create_timer(1), "timeout")
	keypress_enabled = true




func _input(event) -> void:
	if keypress_enabled:
		if (event.is_action_pressed("ui_accept")
			or event.is_action_pressed("ui_select")
			or event.is_action_pressed("ui_cancel")
			or event.is_action_pressed("ui_click")):

			# unless we specify the time it takes as long as it took to get here
			Framework.change_scene(Config.scenes.new_game, true, 0.25)
