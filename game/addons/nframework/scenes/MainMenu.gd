extends Control


onready var button_newgame: Button = $Control/MenuOptions/NewGame
onready var button_settings: Button = $Control/MenuOptions/Settings
onready var button_exit: Button = $Control/MenuOptions/Exit


func _ready():
#	theme = load(Config.theme)

	if Settings.html5:
		button_exit.visible = false

	button_newgame.connect("pressed", self, "_on_NewGame_pressed")
	button_settings.connect("pressed", self, "_on_Settings_pressed")
	button_exit.connect("pressed", self, "_on_Exit_pressed")

	if Config.menu_keyboard_enabled:
		setup_focus()

#	$Control/ColorRect.color.h = 0.6


func setup_focus():
	Utils.setup_focus_grabs_on_mouse_entered($Control/MenuOptions)

	button_newgame.grab_focus()


func _on_NewGame_pressed():
	Framework.change_scene(Config.scenes.new_game, true, 1)


func _on_Settings_pressed():
	Framework.change_scene(Config.scenes.settings_menu)


#func _on_Credits_pressed():
#    pass # Replace with function body.


func _on_Exit_pressed():
	get_tree().quit()


