extends Control


signal menu_closed

enum ControlsDisplay { AUTO, LIST, TABS }

const NTabs = preload("res://addons/nframework/nodes/Tabs.tscn")
const ControlBind = preload("res://addons/nframework/ui/controls/ControlBind.tscn")

# storing a list of all the ControlBind nodes that we have added
# this is a better system of storing things than looping and comparing names
var control_bind_nodes := {}

var _controls_display: int = ControlsDisplay.LIST

onready var controls_list: VBoxContainer = find_node("ControlsList")
onready var controls_tabs: VBoxContainer = find_node("ControlsTabs")
onready var controls_tabs_container: MarginContainer = controls_tabs.get_node("TabsContainer")

onready var rebind_popup: Popup = find_node("RebindPopup")

onready var button_back: Button = find_node("Back")
onready var main_tab_buttons: HBoxContainer = $MarginContainer/Tabs/TabButtons
onready var button_main_tab_controls: Button = main_tab_buttons.get_node("Controls")
onready var control_schemes_tab_buttons: HBoxContainer = controls_tabs.get_node("TabButtons")

onready var button_fullscreen: Button = find_node("Fullscreen")
onready var button_borderless: Button = find_node("Borderless")
onready var button_vsync: Button = find_node("VSync")
onready var button_fps: Button = find_node("FPS")


func _ready():
#	if Settings.html5:
#		button_borderless.visible = false
#		find_node("Scale").visible = false

	setup_gui()

	setup_controls_gui()

	button_fullscreen.pressed = SettingsVideo.fullscreen
	button_borderless.pressed = SettingsVideo.borderless
	button_vsync.pressed = SettingsVideo.vsync
	button_fps.pressed = FPS.enabled

	button_fullscreen.connect("pressed", self, "_on_Fullscreen_pressed")
	button_borderless.connect("pressed", self, "_on_Borderless_pressed")
	button_vsync.connect("pressed", self, "_on_VSync_pressed")
	button_fps.connect("pressed", self, "_on_FPS_pressed")

	button_back.connect("pressed", self, "_on_Back_pressed")

	if Settings.html5:
		button_borderless.visible = false
		# what about vsync and fullscreen?

#	setup_custom_tabs()

	if Config.menu_keyboard_enabled:
		setup_focus()


func setup_gui() -> void:
	theme = load(Config.theme)
	if !Config.low_res:
		Utils.set_margins($MarginContainer, 16)
		Utils.set_margins($MarginContainer/VBoxContainer/MarginContainer, 0, 400)
		for child in find_node("TabsContainer").get_children():
			Utils.set_margins(child.get_node("MarginContainer"), 16, 32)
			pass


func _input(event) -> void:
	if event.is_action_pressed("ui_cancel"):
		button_back.grab_focus()
#		button_back.pressed = true
		yield(get_tree().create_timer(0.15), "timeout")
		_on_Back_pressed()


func setup_focus():
	Utils.setup_focus_grabs_on_mouse_entered($MarginContainer)

#	for button in main_tab_buttons.get_children():
#		button.focus_neighbour_top = button_back.get_path()

	var button_video = main_tab_buttons.get_node("Video")

	button_back.focus_neighbour_top = button_back.get_path()
	button_back.focus_neighbour_right = button_video.get_path()
	button_back.focus_neighbour_bottom = button_video.get_path()
	# disable autograb of focus so we don't get on this button from anywhere else
	# other than what we have already defined
	button_back.focus_mode = Control.FOCUS_CLICK

	button_video.focus_neighbour_left = button_back.get_path()
	button_video.focus_neighbour_top = button_back.get_path()

	button_fullscreen.grab_focus()


func setup_controls_gui() -> void:
	#	if controls_display == ControlsDisplay.AUTO:
	if SettingsControls.control_schemes.size() > 1:
		_controls_display = ControlsDisplay.TABS
	else:
		_controls_display = ControlsDisplay.LIST
#	else:
#		_controls_display = controls_display

	match _controls_display:
		ControlsDisplay.TABS:
			controls_tabs.visible = true

#			var tabs_container = controls_tabs.get_node("TabsContainer")

			for i in SettingsControls.control_schemes.size():
				var control_scheme_name: String = SettingsControls.control_schemes[i]
				var vbox = VBoxContainer.new()
				vbox.name = control_scheme_name.capitalize()
				controls_tabs_container.add_child(vbox)
				control_bind_nodes[control_scheme_name] = {}

		ControlsDisplay.LIST:
			controls_list.visible = true
			var control_scheme_name: String = SettingsControls.control_schemes[0]
			control_bind_nodes[control_scheme_name] = {}

	for action in SettingsControls.action_controls.keys():
		for scheme in SettingsControls.control_schemes:
			add_bind(action, SettingsControls.action_controls[action][scheme], scheme)

	# must be done after binds have been added with add_bind
	if _controls_display == ControlsDisplay.TABS:
		controls_tabs.init()
		setup_controls_tabs_focus()


func setup_controls_tabs_focus():
	for b in control_schemes_tab_buttons.get_children():
		b.connect("toggled", self, "_control_schemes_tab_button_toggled", [b])

	# retrigger the initial toggle
	control_schemes_tab_buttons.get_child(0).pressed = false
	control_schemes_tab_buttons.get_child(0).pressed = true

	for scheme_tab in controls_tabs_container.get_children():
		var control = scheme_tab.get_child(0).get_node("Main/Rebind")
		var tab_button = control_schemes_tab_buttons.get_node(scheme_tab.name)
		tab_button.focus_neighbour_top = tab_button.get_path_to(button_main_tab_controls)
		control.focus_neighbour_top = control.get_path_to(tab_button)


func add_bind(action, control, scheme) -> void:
	var entry = ControlBind.instance()

	if _controls_display == ControlsDisplay.LIST:
		controls_list.add_child(entry)
	else:
		controls_tabs_container.get_node(scheme.capitalize()).add_child(entry)

	control_bind_nodes[scheme][action] = entry
	entry.get_node("Action").text = str(action.capitalize()) # optionally add colon after action
	update_bind(action, control, scheme)


func update_bind(action, event, scheme) -> void:
	var entry = control_bind_nodes[scheme][action]
	entry.get_node("Main/Rebind").text = SettingsControls.get_inputevent_name(event)
	entry.get_node("Main/Rebind").connect("pressed", self, "_on_Rebind_pressed", [action, scheme])

	entry.get_node("Main/Rebind").focus_neighbour_left = entry.get_path()


func _control_schemes_tab_button_toggled(toggled, button):
	if toggled:
		button_main_tab_controls.focus_neighbour_bottom = button_main_tab_controls.get_path_to(button)


func _on_Rebind_pressed(action, scheme) -> void:
	rebind_popup.popup_centered()
	yield(rebind_popup, "control_rebound")
	if rebind_popup.new_event == null:
		return
	var event: InputEvent = rebind_popup.new_event
	SettingsControls.replace_action(action, event, scheme)
	update_bind(action, event, scheme)
	control_bind_nodes[scheme][action].get_node("Main/Rebind").grab_focus()


func _on_Back_pressed() -> void:
	if get_tree().get_root().get_node(self.name): # if we were loaded as a scene
		Framework.change_scene(Config.scenes.main_menu)
	else: # or as an overlay above the pause/game screen
		visible = false
		queue_free()
		emit_signal("menu_closed")


func _on_Fullscreen_pressed() -> void:
	SettingsVideo.fullscreen = button_fullscreen.pressed


func _on_Borderless_pressed() -> void:
	SettingsVideo.borderless = button_borderless.pressed


func _on_VSync_pressed() -> void:
	SettingsVideo.vsync = button_vsync.pressed


func _on_FPS_pressed() -> void:
	FPS.enabled = button_fps.pressed
