tool
extends EditorPlugin

const BASE_PATH: String = "res://addons/nframework/"
const AUTOLOADS_PATH: String = BASE_PATH + "autoloads/"
const NULL_AUTOLOAD: String = "Null.gd"

# order matters!
# the autoloads will be loaded and appear in the scenetree in this order
# you can disable autoloads by adding them to disabled_autoloads
# almost all autoloads require the core modules, so don't disable those
# you mostly just want to disable the overlays
const autoloads := {
	## core modules
	"Log": "Logger.gd",
	"Config": "Config.gd",

	# runtime settings
	"Settings": "Settings.gd",
	"SettingsAudio": "SettingsAudio.gd",
	"SettingsControls": "SettingsControls.gd",
	"SettingsVideo": "SettingsVideo.gd",

	# overlays
	"Transition": "overlay/TransitionOverlay.tscn",
	"Pause": "overlay/PauseMenuOverlay.tscn",
	"FPS": "overlay/FpsOverlay.tscn",

	# systems
	# framework depends on transition and pause overlays
	"Framework": "Framework.gd",
}

const disabled_autoloads := [
#	"Settings",
#	"SettingsAudio",
#	"SettingsControls",
#	"SettingsVideo",	
#	"Pause",
#	"Transition",
#	"FPS"
#	"Framework",
	]


var dock_config: ScrollContainer
var dock_file_editor: Control


func _enter_tree():
	# Initialization of the plugin goes here.
	
	for key in autoloads.keys():
		if key in disabled_autoloads:
			add_autoload_singleton(key, AUTOLOADS_PATH + NULL_AUTOLOAD)
		else:
			add_autoload_singleton(key, AUTOLOADS_PATH + autoloads[key])
		
	# Load the dock. Load from file here so it's reloaded on every _enter_tree ?
	dock_config = preload("res://addons/nframework/dock/ConfigPanel.tscn").instance()
	add_control_to_dock(DOCK_SLOT_LEFT_BR, dock_config)
#    add_control_to_bottom_panel(dock, "NFramework")

	dock_file_editor = preload("res://addons/nframework/dock/FileEditor.tscn").instance()
	add_control_to_dock(DOCK_SLOT_LEFT_BR, dock_file_editor)

func _exit_tree():
	# Clean-up of the plugin goes here.
	
	for key in autoloads.keys():
		remove_autoload_singleton(key)
		
	# Remove the dock.
#    remove_control_from_bottom_panel(dock)
	remove_control_from_docks(dock_config)
	dock_config.queue_free()
	remove_control_from_docks(dock_file_editor)
	dock_file_editor.queue_free()	
