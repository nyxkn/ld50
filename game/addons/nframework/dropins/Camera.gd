extends Camera2D

## the basic way of using a camera is to attach it as a child of what you want to track
## but that isn't very flexible and if you want custom smoothing you might be
## better off manually adjusting the camera position to track what you want to track
## this second approach is what we do here


enum ShakeType {
	ROTATIONAL = 1
	TRANSLATIONAL = 2
}

export(int, FLAGS, "Rotational", "Translational") var shake_type: int = ShakeType.ROTATIONAL | ShakeType.TRANSLATIONAL
export(NodePath) var tracking: NodePath = ""
#export(OpenSimplexNoise) var noise: OpenSimplexNoise = OpenSimplexNoise.new()
# use this line instead if you want to use a noise texture you can visualize for fine tuning
# FIXME eventually remove this and the sprite node
onready var noise: OpenSimplexNoise = $NoiseVisualizer.texture.noise

## trauma units per second
export(float, 0.0, 2.0) var trauma_decay: float = 0.8
## trauma exponent. use [2, 3]. the higher this is, the slower the curve starts
export(int, 2, 3) var trauma_exp: int = 2
## in degrees. at small angle values, it's easier to think in degrees than radians
export(int, 0, 10) var shake_angle_max: int = 5
## pixels
export(Vector2) var shake_offset_max: Vector2 = Vector2(20, 10)
## multiplier for rate of change of perlin noise. 1.0 feels alright but maybe a bit soft
export(float, 0.1, 5.0) var shake_intensity: float = 2.0

## our internal trauma [0,1]
var trauma: float = 0.0
## elapsed time in seconds
var _time: float = 0.0
## shortcut alias
var _rng = Framework.rng


func _ready() -> void:
	noise.seed = _rng.randi()
	# smallest possible period seems to work well with a time input in seconds
	noise.period = 0.1


func _process(delta: float) -> void:
	_time += delta

	# track
	var node := get_node(tracking) as Node2D
	position = node.position

	# decrease trauma linearly over time
	trauma -= trauma_decay * delta
	trauma = clamp(trauma, 0.0, 1.0)

#	var shake_amount = trauma * trauma
	var shake_amount = pow(trauma, trauma_exp)

	if shake_type & ShakeType.ROTATIONAL:
		var angle = deg2rad(shake_angle_max) * shake_amount * get_noise(0)
		rotation = angle
	if shake_type & ShakeType.TRANSLATIONAL:
		var offset_x = shake_offset_max.x * shake_amount * get_noise(1)
		var offset_y = shake_offset_max.y * shake_amount * get_noise(2)
		offset = Vector2(offset_x, offset_y)


func get_noise(seed_offset: int) -> float:
	# plain random generation
#	return _rng.randf_range(-1.0, 1.0)

	# or perlin noise
	# here we are using the y coordinate as a seed value
	# the alternative would be to chanage the seed of the noise generator before every call
	# but that seems a bit more clumsy
	# y has to be large enough so that the distance guarantees you're looking at unrelated data
	# 1000 seems like a safe value, but less would probably also work
	var random = noise.get_noise_2d(_time * shake_intensity, seed_offset * 1000)
	return random


func add_trauma(amount: float) -> void:
	trauma += amount
	trauma = clamp(trauma, 0.0, 1.0)

