extends Node

# necessary ui controls
# left down up right, accept, cancel
# in controllers, this is nowadays X for select, O for cancel
# but in older jrpg it's O for select and X for cancel === A and B in SNES, which makes sense
# (and possibly modern japanese ps systems too?)

#  X    Y     ^
# Y A  X B  [] O
#  B    A     X
# SNES XBOX Playstation

# https://docs.godotengine.org/en/stable/classes/class_%40globalscope.html#enum-globalscope-keylist

# database of the default keybindings for each control scheme
# each control scheme contains a list of controls assigned to each action
const control_scheme_actions := {
	keyboard = {
		"left": KEY_A,
		"down": KEY_S,
		"up": KEY_W,
		"right": KEY_D,
		"action1": KEY_J,
		"action2": KEY_K,
		},
	keyboard_alt = {
		"left": KEY_LEFT,
		"down": KEY_DOWN,
		"up": KEY_UP,
		"right": KEY_RIGHT,
		"action1": KEY_X,
		"action2": KEY_Z,
		},
	joystick = {
		"left": JOY_DPAD_LEFT,
		"down": JOY_DPAD_DOWN,
		"up": JOY_DPAD_UP,
		"right": JOY_DPAD_RIGHT,
		"action1": JOY_SONY_X,
		"action2": JOY_SONY_CIRCLE,
		},
	}

const ui_actions := {
	"left": "ui_left",
	"down": "ui_down",
	"up": "ui_up",
	"right": "ui_right",
	"action1": "ui_accept",
	"action2": "ui_cancel",
	}

# actions defines the actions we make use of in-game
# does not have to match the scheme controls list
var actions := ["left", "down", "up", "right", "action1", "action2", "action3"]
# control schemes defines which schemes we are using
var control_schemes := [ "keyboard", "joystick" ]
#var control_schemes := [ "keyboard" ]
# generated list of all controls assigned to each action
# each action can end up with more than one control
var action_controls := {}


func init() -> void:
	# optionally clear all of godot's default actions
#	clear_all_actions()

	# ideally load from save/config file
	# InputMap is global so it persists everywhere
	for action in actions:
		InputMap.add_action(action)
		action_controls[action] = {}
		# action_controls[action].resize(2)
		for scheme in control_schemes:
			action_controls[action][scheme] = create_action_from_db(action, scheme)

#	print_default_action_keys()
#	print_action_controls()



func replace_action(action, event, scheme) -> void:
	InputMap.action_erase_event(action, action_controls[action][scheme])
	InputMap.action_add_event(action, event)
	action_controls[action][scheme] = event


func clear_all_actions() -> void:
	var actions = InputMap.get_actions()
	for a in actions:
		InputMap.action_erase_events(a)





func create_action_from_db(action, scheme) -> InputEvent:
	var scheme_dict = control_scheme_actions[scheme]

	var action_control
	if action in scheme_dict:
		var event: InputEvent
		if scheme.begins_with("keyboard"):
			event = InputEventKey.new()
			event.scancode = scheme_dict[action]
		elif scheme.begins_with("joystick"):
			event = InputEventJoypadButton.new()
			event.button_index = scheme_dict[action]

		InputMap.action_add_event(action, event)
		# also add the key to the ui navigation keys
		InputMap.action_add_event(ui_actions[action], event)
		action_control = event

	else:
		action_control = InputEventKey.new()

	return action_control


func print_default_action_keys() -> void:
	for a in InputMap.get_actions():
		# exclude/choose ui_ default mappings
		if a.left(3) == 'ui_':
			var log_str = str(a, ": ")
			for i in InputMap.get_action_list(a):
				log_str += str(i.as_text(), " | ")
			log_str = log_str.trim_suffix(" | ")
			Log.d(log_str, "Input Default")


func print_action_controls() -> void:
	pass
	for action in action_controls.keys():
		var log_str = str(action, ": ")
		for control in action_controls[action].values():
#			log_str += str(key.as_text(), " ")
			log_str += str(control.as_text(), " | ")
		log_str = log_str.trim_suffix(" | ")
		Log.d(log_str, "Input")


# function lifted from GameTemplate
func get_inputevent_name(event: InputEvent) -> String:
	var text: String = ""
	if event is InputEventKey:
#		text = "Keyboard: " + event.as_text()
		text = event.as_text()
	elif event is InputEventJoypadButton:
#		text = "Gamepad: "
#		if Input.is_joy_known(event.device):
#			text+= str(Input.get_joy_button_string(event.button_index))
#		else:
#			text += "Btn. " + str(event.button_index)
		text+= str(Input.get_joy_button_string(event.button_index))
	elif event is InputEventJoypadMotion:
#		text = "Gamepad: "
		var stick := ''
		if Input.is_joy_known(event.device):
			stick = str(Input.get_joy_axis_string(event.axis))
			text += stick + " "
		else:
			text += "Axis: " + str(event.axis) + " "

		if !stick.empty(): #known
			var value: int = round(event.axis_value)
			if stick.ends_with('X'):
				if value > 0:
					text += 'Rigt'
				else:
					text += 'Left'
			else:
				if value > 0:
					text += 'Down'
				else:
					text += 'Up'
		else:
			text += str(round(event.axis_value))

	return text
