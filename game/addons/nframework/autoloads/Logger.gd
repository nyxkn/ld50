tool
extends Node

## Consider just using godot-logger instead
## https://github.com/KOBUGE-Games/godot-logger

# enum Category {
# #    SCREEN_MANAGER, MENU, PERSISTENCE, MAP, GAME, STATE, SIGNAL, PLAYER
# 	FRAMEWORK, CONFIG, GAME, PRINT
# }


enum Level {
	# debug - always meant to be disabled in release
	# VERBOSE, # from godot-logger
	# TRACE, # log4j
	DEBUG, # prints used for debugging behaviour or tracking things

	# informational - these stay in release. they provide meaningful information to the user
	INFO, # inform the user/yourself of something
	WARN, # provide a warning
	ERROR, # notify of an error
	# FATAL, # log4j
}

# var default_category = "main"

# Some of this code is taken from godot-logger
# https://github.com/KOBUGE-Games/godot-logger/blob/master/logger.gd

# e.g. "[INFO] [main] The young alpaca started growing a goatie."
var output_format = "[{TIME}] ({TICKS}) [{LVL}] [{CAT}] {MSG}"
var output_format_no_category = "[{TIME}] ({TICKS}) [{LVL}] {MSG}"

# Example with all supported placeholders: "YYYY.MM.DD hh.mm.ss"
# would output e.g.: "2020.10.09 12:10:47".
var time_format = "hh:mm:ss.ms"


# Format the fields:
# * YYYY = Year
# * MM = Month
# * DD = Day
# * hh = Hour
# * mm = Minutes
# * ss = Seconds
# * ms = Milliseconds
func get_formatted_datetime():
#	var datetime = OS.get_datetime()
	var msec_epoch := OS.get_system_time_msecs()
	var datetime = OS.get_datetime_from_unix_time(int(msec_epoch * 0.001))
	var msec = msec_epoch % 1000

	var result = time_format
	result = result.replace("YYYY", "%04d" % [datetime.year])
	result = result.replace("MM", "%02d" % [datetime.month])
	result = result.replace("DD", "%02d" % [datetime.day])
	result = result.replace("hh", "%02d" % [datetime.hour])
	result = result.replace("mm", "%02d" % [datetime.minute])
	result = result.replace("ss", "%02d" % [datetime.second])
	result = result.replace("ms", "%03d" % msec)

	return result


func get_formatted_ticks():
	var ticks = OS.get_ticks_usec()
	var seconds = ticks * 0.000001
	return Utils.format_seconds(seconds, Utils.SecondsResolution.US)


func format(level, category, message):
	var output = output_format
	if category == "": output = output_format_no_category

	output = output.replace("{LVL}", Level.keys()[level])
	output = output.replace("{CAT}", category)
	output = output.replace("{MSG}", str(message))
	output = output.replace("{TIME}", get_formatted_datetime())
	output = output.replace("{TICKS}", get_formatted_ticks())

	return output


## category can be a string or a value from Category enum
## objects can be an array or a single basic type
func log(objects, category: String, level: int = Level.DEBUG):
	# Don't log if globally off
	if !Config.log_show: return

	# Don't log if this Category selectively hidden
	# if Config.HIDE_LOG_CATEGORY.has(category) and Config.HIDE_LOG_CATEGORY[category] == true: return

	# Don't log if this Level selectively hidden
	if Config.log_hide_level.has(level) and Config.log_hide_level[level] == true: return

	# Convert category index or string
	# var category_str = ""
	# if category is String:
	# 	# category_str = category.to_upper()
	# 	category_str = category
	# elif category is int and Category.values().has(category):
	# 	category_str = str(Category.keys()[category]).to_lower()

	# Format objects depending on type
	var objects_str = ""
	if objects is Array:
		for obj in objects:
			objects_str += str(obj) + " "
		objects_str = objects_str.trim_suffix(" ")
	else:
		objects_str = str(objects)

	var result_str = format(level, category, objects_str)

	if level == Level.ERROR:
		printerr(result_str)
		push_error(result_str)
	elif level == Level.WARN:
		print(result_str)
		push_warning(result_str)
	else:
		print(result_str)


func d(objects, category = ""):
	self.log(objects, category, Level.DEBUG)


func i(objects, category = ""):
	self.log(objects, category, Level.INFO)


func w(objects, category = ""):
	self.log(objects, category, Level.WARN)


func e(objects, category = ""):
	self.log(objects, category, Level.ERROR)


# generic print. can be used instead of print()
func p(objects):
	self.log(objects, "print", Level.DEBUG)
