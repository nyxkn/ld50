extends CanvasLayer


signal fade_out_completed
signal fade_in_completed

const EASE: int = Tween.EASE_IN

export var duration: float = 0.25
export var fade_in_alpha: float = 0.0
export var fade_out_alpha: float = 1.0

var fade_percent: float = 0 setget set_fade_percent

var fading: float = false

onready var black_screen := $BlackScreen
onready var tween_out := $TweenOut
onready var tween_in := $TweenIn


func _ready() -> void:
	black_screen.modulate.a = 0

# if you need to drop this scene into another, you can set colorrect.color to alpha0 in the inspector
# and enable this here so the black screen doesn't get in the way
#	black_screen.color.a = 1


func fade_out(custom_duration: float = 0.0) -> void:
	fading = true
	if custom_duration > 0: duration = custom_duration
	tween_out.interpolate_property(self, "fade_percent", fade_in_alpha, fade_out_alpha, duration, Tween.TRANS_LINEAR, EASE)
	tween_out.start()


func fade_in(custom_duration: float = 0.0) -> void:
	if custom_duration > 0: duration = custom_duration
	tween_in.interpolate_property(self, "fade_percent", fade_out_alpha, fade_in_alpha, duration, Tween.TRANS_QUAD, EASE)
	tween_in.start()


# you could be animating either color or modulate
# color: only changes the colorrect color - in this case you need canvas to be on a higher layer
# modulate: (property of canvasitem) modulates everything drawn on that canvas. can be on same layer as the rest
# e.g. children of colorrect as well - can be more versatile
func set_fade_percent(value: float) -> void:
	fade_percent = clamp(value, 0.0, 1.0)
	# fade logic
	black_screen.modulate.a = fade_percent


func _on_TweenOut_tween_all_completed() -> void:
	# faded out
	emit_signal("fade_out_completed")


func _on_TweenIn_tween_all_completed() -> void:
	# faded in
	emit_signal("fade_in_completed")
	fading = false
