extends CanvasLayer


var enabled: bool setget set_enabled

var update_interval: float = 1
var timer: float

onready var main_control: Control = $Main


func _ready() -> void:
	main_control.theme = load(Config.theme)
	set_enabled(false)
	main_control.get_node("FPS").text = ""


func _process(delta: float) -> void:
	timer += delta
	if timer > update_interval:
		timer = 0
		main_control.get_node("FPS").text = str(Engine.get_frames_per_second())


func set_enabled(value: bool) -> void:
	enabled = value
	set_process(enabled)
	main_control.visible = enabled
