extends CanvasLayer

# public
var paused: bool = false setget toggle_paused
# each scene is responsible for enabling pausing_allowed.
# it is automatically disabled in Framework.change_scene()
var pausing_allowed: bool = false setget set_pausing_allowed

# private
var settings_opened: bool = false
var settings_menu: Control = null

onready var SettingsMenu = load(Config.scenes.settings_menu)

onready var button_resume: Button = find_node("Resume")
onready var main_control: Control = $Main


func _ready():
	main_control.theme = load(Config.theme)
	main_control.visible = false
	Utils.setup_focus_grabs_on_mouse_entered(main_control)


func _input(event) -> void:
	# _input is still called despite the scenetree being paused
	# because pause_mode of this node is set to process
	if event.is_action_pressed("ui_cancel"):
		get_tree().set_input_as_handled()
		if paused:
			if !settings_opened:
				self.paused = false
		else:
#			Log.d("pausing", name)
			self.paused = true


func set_pausing_allowed(value: bool) -> void:
	pausing_allowed = value
	set_process_input(pausing_allowed)
	if pausing_allowed and Config.ludum_dare:
		Log.d("are pre-made menus allowed for ludum dare?", name)


func toggle_paused(value: bool) -> void:
	paused = value
#	Log.d(str("toggle_paused: ", paused), name)
	# this also pauses all input
	get_tree().paused = paused
	main_control.visible = paused
	if paused:
		button_resume.grab_focus()


func _on_Resume_pressed():
	Log.d("resume pressed", name)
	self.paused = false


func _on_MainMenu_pressed() -> void:
	Framework.change_scene(Config.scenes.main_menu)

	# TransitionOverlay has to be set to process during pause for this to work
	# otherwise just do the change_scene without transitions
	yield(Framework, "scene_faded_out")
	self.paused = false


func _on_Settings_pressed() -> void:
	# we have to hide this so it doesn't conflict with gui navigation in settings
	main_control.get_node("PauseMenu").hide()

	settings_opened = true
	settings_menu = SettingsMenu.instance()
	settings_menu.connect("menu_closed", self, "on_SettingsMenu_closed")
	main_control.add_child(settings_menu)



func on_SettingsMenu_closed() -> void:
#	remove_child(settings_menu)
#	settings_menu.queue_free()
	main_control.get_node("PauseMenu").show()
	button_resume.grab_focus()
	settings_opened = false
