extends Node


var bus_ids := {}

# we store volumes as [0,1] floats
# godot instead wants a db value for the volume
var volumes := {}


func init() -> void:
	# AudioServer bus index is an incremental int starting at 0
	# so if bus_count is 3, we have 3 buses with ids 0, 1, 2
	for i in AudioServer.bus_count:
		bus_ids[AudioServer.get_bus_name(i)] = i

	# print(bus_ids)

	# read volumes from the godot editor
	for bus in bus_ids:
		var volume = db2linear(AudioServer.get_bus_volume_db(bus_ids[bus]))
		set_volume(bus, volume)


# we use bus name as our id, rather than AudioServer's int id
func set_volume(bus: String, volume: float) -> void:
	if not bus in bus_ids.keys():
		Log.e(["attempting to set volume of inexistent bus", bus], name)
		return

	volumes[bus] = volume
	AudioServer.set_bus_volume_db(bus_ids[bus], linear2db(volume))


# a float value [0,1]
func get_volume(bus: String) -> float:
	if not bus in bus_ids.keys():
		Log.e(["attempting to get volume of inexistent bus", bus], name)
		return float(-1)

	return volumes[bus]

