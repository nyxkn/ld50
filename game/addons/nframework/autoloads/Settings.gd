extends Node

# OS
# does this belong in here or in Config?
# here makes more sense because it's a runtime variable
# but at the same time it's not one we would want to persist
var html5: bool = false

## we want to call these things in here to guarantee order. don't use _ready in the individual scripts

func _ready()->void:
	if OS.get_name() == "HTML5":
		html5 = true
#	SettingsLanguage.add_translations() #TO-DO need a way to add translations to project through the plugin
	SettingsVideo.init()
#	if !SettingsSaveLoad.load_settings():
#		SettingsControls.default_controls()
	SettingsControls.init()
	SettingsAudio.init()
	#SettingsSaveLoad.save_settings() #Call this method to trigger Settings saving
