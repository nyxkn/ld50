tool
extends Node
## this file keeps all configuration. all of these things could be saved to a .cfg file for reloading

################################################################
# LOGGING

# Global switch for showing logs
var log_show: bool = true

# All LogCategories are shown by default. Add true to this Dictionary to
# prevent showing  Logs of this Log Category
# removed for now
# var HIDE_LOG_CATEGORY = {}

# All LogLevels are shown by default. Add true to this Dictionary to
# prevent showing Logs of this Log Level
var log_hide_level := {}


################################################################
# RESOURCES

var scenes := {
	intro = "res://addons/nframework/scenes/Intro.tscn",
	ending = "res://addons/nframework/scenes/Ending.tscn",
	main_menu = "res://addons/nframework/scenes/MainMenu.tscn",
	settings_menu = "res://addons/nframework/scenes/SettingsMenu.tscn",

	# this should point to your game actual entry point
	new_game = "res://Game.tscn"
	}

var theme: String

var levels_path: String = "res://levels/"

################################################################
# OTHER

var low_res: bool = true
# enable or disable menu keyboard controls
var menu_keyboard_enabled: bool = true

#var PAUSE_ENABLED: bool = true

## Ludum Dare mode
## - disable loading of pre-made assets
## starting with framework/library type of base-code is allowed
## what about menus? a "logo/intro screen" is allowed, but maybe menus are not?
var ludum_dare: bool = true

################################################################
# LOGIC

func _ready() -> void:
	if low_res:
		theme = "res://addons/nframework/assets/themes/theme_lowres_tex.tres"
	else:
		# ideally theme_hires would just be an override on top of lowres, without redefining everything
		# should be possible in godot 4
		theme = "res://addons/nframework/assets/themes/theme_hires_tex.tres"

