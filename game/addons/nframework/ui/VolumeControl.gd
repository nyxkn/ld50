tool
extends Control


export(bool) var use_node_name_as_label: bool = false
export(String) var bus_name: String = "Master"

var label: String = ""


func _ready() -> void:
	var bus_volume = SettingsAudio.get_volume(bus_name)
	assert(bus_volume != -1, str("specified bus name (", bus_name, ") does not exist"))

	if use_node_name_as_label:
		label = name
	else:
		label = bus_name

	$MenuSlider/Label.text = label
	$AudioStreamPlayer.bus = bus_name

	$MenuSlider/HSlider.value = bus_volume
	$MenuSlider/HSlider.connect("value_changed", self, "_on_value_changed")


func _on_value_changed(value):
	SettingsAudio.set_volume(bus_name, value)
	$AudioStreamPlayer.play()
