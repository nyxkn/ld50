extends Control

const ControlDescription = preload("res://addons/nframework/ui/controls/ControlDescription.tscn")

const actions_description := {
	left = 'move left',
	action1 = 'attack',
	action2 = 'jump',
	}

func _ready() -> void:
	for action in actions_description:
		var controls = SettingsControls.action_controls[action]
		var control_description = ControlDescription.instance()
		control_description.init(controls['keyboard'], actions_description[action])
		$GridContainer.add_child(control_description)
			
