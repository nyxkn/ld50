extends HBoxContainer


const textures_path = "res://assets/temp/controls/"

#onready var keyName = $MarginContainer/KeyName
onready var label_keyname = $MarginContainer/KeyName


func _ready() -> void:
	pass


func init(control, description) -> void:
	var texture = load(textures_path + control.as_text() + ".tres")

	if not texture or Config.ludum_dare:
		$KeyTexture.hide()
		label_keyname.text = control.as_text()
		label_keyname.show()
	else:
		$KeyTexture.show()
		$KeyTexture.texture = texture
		label_keyname.hide()

	$Description.text = description
