extends Popup

# FIXME this containers structure is horrifying. please find a better solution
# could be either having a fixed size margin container to keep it in the middle (you can still use ratios of screensize in code)
# or avoid containers and choose the size of the popup (you can use ratios of screen size in code)

signal control_rebound

var new_event: InputEvent


func _ready() -> void:
	popup_exclusive = true
	set_process_input(false)
	connect("about_to_show", self, "receive_input")


func receive_input() -> void:
	set_process_input(true)
	get_focus_owner().release_focus()


func _input(event) -> void:
	# TODO: Joypad stuff is untested
	if ! event is InputEventKey && ! event is InputEventJoypadButton && ! event is InputEventJoypadMotion:
		return # only continue if we get one of these
	if !event.is_pressed():
		return
	new_event = event
	emit_signal("control_rebound")
	set_process_input(false)
	visible = false


func _on_Cancel_pressed():
	new_event = null
	emit_signal("control_rebound")
	set_process_input(false)
	visible = false
