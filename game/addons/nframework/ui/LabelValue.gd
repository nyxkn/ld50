tool
#class_name LabelValue
extends HBoxContainer


# these setters get called on scene reload as long as a custom value was provided
# they won't get called on reload if the value is the default empty string
export(String) var label: String = "" setget set_label
export(String) var value: String = "" setget set_value
export(String) var separator: String = "" setget set_separator


func _ready() -> void:
	$Separator.visible = false
	# calling set_label so we update the label to "name" if no label was provided
	set_label(label)


func set_label(value) -> void:
	label = str(value)
	if label:
		$Label.text = value
	else:
		$Label.text = name


func set_value(v) -> void:
	value = str(v)
	$Value.text = value


func set_separator(value) -> void:
	separator = str(value)
	if separator:
		$Separator.text = separator
		$Separator.show()
	else:
		$Separator.hide()
