class_name Math

## A collection of useful math functions

## compute the greatest common divisor
## with euclidean algorithm
## https://en.wikipedia.org/wiki/Euclidean_algorithm#Implementations
static func gcd(a: int, b: int) -> int:
	if b == 0:
		return a
	else:
		return gcd(b, a % b)
