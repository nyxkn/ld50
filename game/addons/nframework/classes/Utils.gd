class_name Utils

## Generic utility functions
## Split into appropriate files if this becomes too large




## simple benchmark function. call this for 10-20 times to simulate a couple seconds lock
static func benchmark_function() -> void:
	var lst = []
	for i in 999999:
		lst.append(sqrt(i))


static func set_margins(margin_container, margin_value: int, margin_rightleft: int = -1) -> void:
	var mc: MarginContainer = margin_container

	if margin_rightleft >= 0:
		mc.add_constant_override("margin_right", margin_rightleft)
		mc.add_constant_override("margin_left", margin_rightleft)
	else:
		mc.add_constant_override("margin_right", margin_value)
		mc.add_constant_override("margin_left", margin_value)

	mc.add_constant_override("margin_top", margin_value)
	mc.add_constant_override("margin_bottom", margin_value)


## store all children of a node into a given array
## node: node to recurse into
## children: array to store children in
static func store_children_recursive(node: Node, children: Array = [], recurse_level: int = 0) -> void:
#	Log.d("    ".repeat(recurse_level) + "[" + node.name + "]")

	for n in node.get_children():
		if n.get_child_count() > 0:
			children.append(n)
			store_children_recursive(n, children, recurse_level + 1)
		else:
			children.append(n)
#			Log.d("    ".repeat(recurse_level + 1) + "- " + n.name)


static func grab_focus_on_mouse_entered(control: Control) -> void:
	control.connect("mouse_entered", control, "grab_focus")


static func setup_focus_grabs_on_mouse_entered(control: Control) -> void:
	var children: Array = []
	store_children_recursive(control, children)

	for c in children:
		if c is Button or c is Slider:
			grab_focus_on_mouse_entered(c)


## look into a dictionary to find the desired value. return its key.
static func dict_get_key_of_value(dict: Dictionary, value_search):
	for k in dict.keys():
		if k == value_search:
			return k
	return null


## format a number of seconds into hh:mm:ss
## optionally also adding the fractional part in ms or us
## you might want to move this to a time class
enum SecondsResolution { S, MS, US }
static func format_seconds(seconds: float, resolution: int = SecondsResolution.MS):
	# extract the fractional part
	var frac = seconds - int(seconds)
	if resolution == SecondsResolution.MS:
		frac = frac * 1000
		frac = "%03d" % frac
	elif resolution == SecondsResolution.US:
		frac = frac * 1000000
		frac = "%06d" % frac

	var seconds_whole = int(floor(seconds))

	var s = seconds_whole % 60
	var m = (seconds_whole / 60) % 60
	var h = seconds_whole / (60 * 60) # add %24 if you want to cap at hours and add days

	var colon := ":"
	var pad := "%02d"

	var formatted = str("+", h, colon, pad % m, colon, pad % s)
	if resolution != SecondsResolution.S: formatted += str(".", frac)

	return formatted


## logging of input events in a sane non-overwhelming way
static func log_event(event: InputEvent, function, name):
	if event is InputEventMouseButton and event.is_pressed():
		Log.d([event.get_class(), function], name)
		# Log.d([event.as_text(), function], name)
		if name == "InputThings": print("--------")
	# elif event is InputEventMouseMotion:
	# 	Log.d([event.get_class(), function], name)
	elif not event is InputEventMouseMotion and event.is_pressed():
		Log.d([event.as_text(), function], name)
		if name == "InputThings": print("--------")


## reparent node onto new parent
## optionally update the ownership to the new parent as well
## this isn't generally needed
static func reparent(child: Node, new_parent: Node, reset_owner: bool = false) -> void:
	var old_parent = child.get_parent()
	old_parent.remove_child(child)
	new_parent.add_child(child)
	if reset_owner: child.owner = new_parent


## read content from file
static func read_file(file_path: String, password: String = ""):
	Log.i(["reading from file:", file_path], "Utils.read_file")
	var file = File.new()

	var err
	if password:
		err = file.open_encrypted_with_pass(file_path, File.READ, password)
	else:
		err = file.open(file_path, File.READ)

	if err != OK:
		Log.e(["error", err, "opening file"], "Utils.read_file")
		return

	var content = file.get_as_text()
	file.close()

	return content


## write content to file
static func write_to_file(content: String, file_path: String, password: String = ""):
	Log.i(["writing to file:", file_path], "Utils.write_to_file")
	var file = File.new()

	var err
	if password:
		err = file.open_encrypted_with_pass(file_path, File.WRITE, password)
	else:
		err = file.open(file_path, File.WRITE)

	if err != OK:
		Log.e(["error", err, "opening file"], "Utils.write_to_file")
		return

	file.store_string(content)
	file.close()


## return a random color
static func random_color() -> Color:
	return Color(rand_range(0,1), rand_range(0,1), rand_range(0,1))


################
# DEPRECATED
################

## pretty print array
static func array_to_printable_string(array) -> String:
	var string := ""
	for e in array:
		string += str(e, ", ")
	return string.substr(0, string.length() - 2)


# i have no idea what use we might have had for this
# this would ideally be a variadic function
static func debug_print(varargs_array) -> void:
	print(array_to_printable_string(varargs_array))
