class_name Data

## Helper class for importing data

## This takes [ [ myname, mytype ] ]
## And turns it into [ { name: myname, type: mytype } ]
## Items are referenced by a number id
## This provides efficient storing and retrieval, but you're not able to access items by named id
static func labelled_fields_from_array(var data: Array, var fields: Array) -> Array:
	var array := []
	array.resize(data.size())
	# loop lines
	for l in data.size():
		array[l] = {}
		# loop columns
		for c in fields.size():
			var field_name = fields[c]
			array[l][field_name] = data[l][c]

	return array


## This takes { id = [ myname, mytype ] }
## And turns it into { id = { name: myname, type: mytype, id: id } }
## Items are referenced by a named id
## Use this instead of labelled_fields_from_array() if you want to access items through a named id
## Requires you to add an additional named id field to all your data
## You can easily derive the standard array above by only taking dict.values()
static func labelled_fields_from_dict(var data: Dictionary, var fields: Array) -> Dictionary:
	var dict := {}
	for id in data:
		dict[id] = {}
		# we can optionally add the id key itself as a value
		# so if you only have the object, you can reverse lookup the id easily
		# dict[id].id = id
		for c in fields.size():
			var field_name = fields[c]
			dict[id][field_name] = data[id][c]

	return dict


static func _print_file_error(file_path: String, err: int):
	Log.e(str("error code ", err, " while opening file: ", file_path), "Data")

## Import csv data into an array
## This assumes the first line of the csv is a line with the field names
## returns either a dictionary with every entry's key being id
## or a simple array of dictionaries
static func import_csv(file_path: String, id_as_key: bool = true, delim: String = ";", trim_empty_lines: bool = true):
	var csv_data := []
	var file = File.new()

	var err = file.open(file_path, file.READ)
	if err != OK:
		_print_file_error(file_path, err)
		return []

	while !file.eof_reached():
		var csv_line: Array = file.get_csv_line(delim)
		# skip line if the first field is null
		if trim_empty_lines and csv_line[0] == "":
			continue
		csv_data.push_back(csv_line)

	# don't know why get_csv_line() ends up adding an extra empty line at the end
	# removing it
	if csv_data.back().size() == 1 and csv_data.back()[0] == "":
		Log.d("csv import: removing extra empty line added at the end", "Data")
		csv_data.pop_back()
	else:
		Log.d("csv import: no need for removing extra empty line added at the end. investigate", "Data")

	file.close()

	# finished reading the csv. now let's generate our properly formatted data variable
	var fields = csv_data[0]
	var data = csv_data.slice(1, csv_data.size())

	if id_as_key:
		var dict_data := {}

		for line in data:
			dict_data[ line.pop_front() ] = line

		# remove id field from fields
		fields.pop_front()

		var formatted_data: Dictionary = labelled_fields_from_dict(dict_data, fields)
		return formatted_data
	else:
		var formatted_data: Array = labelled_fields_from_array(data, fields)
		return formatted_data



## Import castledb data into a dictionary
## If id_as_key = true, use the id value as a key to access your item data
static func import_castledb(file_path: String, id_as_key: bool = true) -> Dictionary:
	var file = File.new()

	var err = file.open(file_path, file.READ)
	if err != OK:
		_print_file_error(file_path, err)
		return {}

	var cdb_data = parse_json(file.get_as_text())

	file.close()

	# now let's generate our properly formatted data variable
	# if id_as_key, this assumes the first column is an identification column
	# you probably want to name this "id" or "name"
	# a single word string is ideal but can technically also contain a long "Item Name"
	# it's simply the field by which we'll be accessing our data
	var sheets := {}
	for sheet in cdb_data.sheets:
		var sheet_name: String = sheet.name
		var sheet_columns: Array = sheet.columns # array of columns names and data types
		var id_column_name = sheet_columns[0].name

		if id_as_key:
			var sheet_data: Dictionary = {}
			for line in sheet.lines:
				var entry = line.duplicate()
				entry.erase(id_column_name)
				sheet_data[ line[id_column_name] ] = entry

			sheets[sheet_name] = sheet_data
		else:
			sheets[sheet_name] = sheet.lines

	return sheets
