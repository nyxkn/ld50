class_name Resolution

## resolutions in godot are stored as Vector2

var w: int
var h: int
var aspect_ratio: Fraction
var description: String


func _init(w: int, h: int, description: String = ""):
	self.w = w
	self.h = h
	self.description = description
	var fraction = Fraction.new(w, h)
	aspect_ratio = fraction.simplified()


func _to_string() -> String:
	return "%sx%s" % [w, h]


func aspect_ratio_string() -> String:
	return str(aspect_ratio).replace("/", ":")


func aspect_ratio_float() -> float:
	return aspect_ratio.decimal()
